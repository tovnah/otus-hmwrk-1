Домашнее задание `Основы работы с Kubernetes (часть 2)`

- выполнить в корневой директории ```kubectl apply -f . ```
- URL для прооверки
 ```curl -H'Host: arch.homework' http://192.168.39.248/otusapp/abolotov/health ```
 - Так же  можно запустить тест в postman с помощью [otuse_homework1.health_test.json](postman/otuse_homework1.health_test.json)